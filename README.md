# RPI cockroachdb

* Master: [![pipeline status](https://gitlab.com/zeiot/rpi-cockroachdb/badges/master/pipeline.svg)](https://gitlab.com/zeiot/rpi-cockroachdb/commits/master)

Docker image of [cockroachdb][] to use on a [Raspberry PI][].

Download sources :

    wget https://binaries.cockroachdb.com/cockroach-v2.1.0-alpha.20180507.src.tgz

Make the ARM :

    $ mkdir -p $GOPATH/src/github.com/cockroachdb/
    $ mv cockroach-v2.1.0-alpha.20180507/src/github.com/cockroachdb/cockroach/ $GOPATH/src/github.com/cockroachdb/
    $ cd $GOPATH/src/github.com/cockroachdb/cockroach
    $ ./build/builder.sh make TYPE=release-aarch64-linux
    $ cp

Then build :

    $ make build version=x.x


# Supported tags

* 1.1.x: [![](https://images.microbadger.com/badges/version/zeiot/rpi-cockroachdb:1.1.3.svg)](https://microbadger.com/images/zeiot/rpi-cockroachdb:1.1.3 "Get your own version badge on microbadger.com")
* 1.0.x: [![](https://images.microbadger.com/badges/version/zeiot/rpi-cockroachdb:1.0.6.svg)](https://microbadger.com/images/zeiot/rpi-cockroachdb:1.0.6 "Get your own version badge on microbadger.com")


## License

See [LICENSE](LICENSE) for the complete license.


## Changelog

A [ChangeLog.md](ChangeLog.md) is available.


## Contact

Nicolas Lamirault <nicolas.lamirault@gmail.com>


[Raspberry PI]: https://www.raspberrypi.org/
[cockroachdb]: https://cockroachdb.io/
